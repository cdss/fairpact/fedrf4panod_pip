import unittest

from sklearn.tree import BaseDecisionTree
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

def compare_trees(
    test_obj: unittest.TestCase,
    trees1: list[BaseDecisionTree],
    trees2: list[BaseDecisionTree],
    assertEqual: bool = True,
) -> None:
    """
    Helper method which compares two lists of trees with each other to check whether they are identical.
    For this purpose, feature importances, children & values are compared with each other.

    Parameters
    ----------
    test_obj: unittest.TestCase
        Object of the current test suite

    trees1: list[BaseDecisionTree]
        First list of decision trees to compare

    trees2: list[BaseDecisionTree]
        Second list of decision trees to compare

    Returns
    -------
    None

    """
    # Check identical length (prerequisite)
    test_obj.assertEqual(len(trees1), len(trees2))
    for i in range(len(trees1)):
        # Access the respective trees of the two lists
        current_dt1 = trees1[i]
        current_tree1 = trees1[i].tree_
        current_dt2 = trees2[i]
        current_tree2 = trees2[i].tree_
        if assertEqual:
            # Checks various features of the trees to compare equality
            test_obj.assertEqual(
                current_dt1.feature_importances_.tolist(),
                current_dt2.feature_importances_.tolist(),
            )
            test_obj.assertEqual(
                current_tree1.children_left.tolist(),
                current_tree2.children_left.tolist(),
            )
            test_obj.assertEqual(
                current_tree1.value.tolist(), current_tree2.value.tolist()
            )
        elif not assertEqual:
            # Checks various features of the trees to compare equality
            test_obj.assertNotEqual(
                current_dt1.feature_importances_.tolist(),
                current_dt2.feature_importances_.tolist(),
            )
            test_obj.assertNotEqual(
                current_tree1.children_left.tolist(),
                current_tree2.children_left.tolist(),
            )
            test_obj.assertNotEqual(
                current_tree1.value.tolist(), current_tree2.value.tolist()
            )


def calculate_metrics(y_true, y_pred):
    """
    Calculate metrics for the given true and predicted labels

    Parameters
    ----------
    y_true: array-like
        The true labels

    y_pred: array-like
        The predicted labels

    Returns
    -------
    dict
        A dictionary containing the calculated metrics

    """

    metrics = {
        "accuracy": accuracy_score(y_true, y_pred),
        "precision": precision_score(y_true, y_pred),
        "recall": recall_score(y_true, y_pred),
        "f1": f1_score(y_true, y_pred),
    }

    print(metrics)

    # assert all metrics are between 0 and 1
    assert all(0 <= metric <= 1 for metric in metrics.values())